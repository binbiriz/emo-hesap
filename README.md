# EMO Elektrik Faturası Hesabı Sayfası

Sayfaya [bu linkten ulaşabilirsiniz](https://binbiriz.gitlab.io/emo-hesap/).

[iframe örneği için tıklayınız](https://binbiriz.gitlab.io/emo-hesap/iframe.html).

## Open in GitPod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/binbiriz/emo-hesap)

## References

[Gulp 4: A Sample Project](https://sharkcoder.com/tools/gulp) is used for `gulp watch` and `browser-sync` implementation.
