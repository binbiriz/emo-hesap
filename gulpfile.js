const { watch, parallel, series } = require("gulp");

const browsersync = require('browser-sync').create();

var gulp = require("gulp"),
  // concat = require("gulp-concat"),
  // order = require("gulp-order"),
  gulpdata = require("gulp-data"),
  handlebars = require("gulp-compile-handlebars"),
  rename = require("gulp-rename"),
  clean = require("gulp-clean"),
  paths = {
    styles: {
      // By using styles/**/*.sass we're telling gulp to check all folders for any sass file
      src: "src/style.scss",
      srcw: ["src-html/**/*.hbs"],
      srcwroot: ["src-html/*.hbs"],
      // Compiled files will end up in whichever folder it's found in (partials are not compiled)
      dest: "dist",
    },
    concatJS: ["src-js/script.js", "src-js/bikalite.js"],
    // Easily add additional paths
    // ,html: {
    //  src: '...',
    //  dest: '...'
    // }
  },
  templateData = {},
  options = {
    ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false
    partials: {
      footer: "<footer>the end</footer>",
    },
    batch: ["./src-html/partials/"],
    helpers: {
      capitals: function (str) {
        return str.toUpperCase();
      },
    },
  };

function distClean() {
  return gulp.src("dist", { read: false, allowEmpty: true }).pipe(clean());
}

exports.distClean = distClean;

function htmlCompile() {
  // Where should gulp look for the sass files?
  // My .sass files are stored in the styles folder
  // (If you want to use scss files, simply look for *.scss files instead)
  // var veriler = require('require-all')(__dirname + '/data');
  //console.log(veriler);
  return (
    gulp
      .src(paths.styles.srcwroot)
      // Use sass with the files found, and log any errors
      .pipe(handlebars(templateData, options))
      // What is the destination for the compiled file?
      .pipe(
        rename(function (path) {
          path.extname = ".html";
        })
      )
      .pipe(gulp.dest("dist"))
      .pipe(browsersync.stream())
  );
}
// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style

exports.htmlCompile = htmlCompile;

exports.html = series(distClean, htmlCompile);

function watchFiles() {
  //I usually run the compile task when the watch task starts as well
  // distClean();
  // htmlCompile();
  watch(paths.styles.srcw, series(distClean, htmlCompile));
}

// BrowserSync

function browserSync() {
  browsersync.init({
    server: {
      baseDir: './dist/',
      index: "index.html"
    },
    port: 3000
  });
}

// Don't forget to expose the task!
exports.watch = series(series(distClean, htmlCompile), parallel(watchFiles, browserSync));
